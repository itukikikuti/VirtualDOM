let DiffType = {
    None: 0,
    Type: 1,
    Text: 2,
    Node: 3,
    Value: 4,
    Attribute: 5
}

function h(nodeName, attributes, children) {
    return { nodeName, attributes, children }
}

function createElement(node) {
    if (!isNode(node))
        return document.createTextNode(node.toString())

    const e = document.createElement(node.nodeName);

    for (let attribute in node.attributes) {
        if (isEventAttribute(attribute)) {
            const eventName = attribute.slice(2)
            e.addEventListener(eventName, node.attributes[attribute])
        } else {
            e.setAttribute(attribute, node.attributes[attribute])
        }
    }

    for (let child of node.children) {
        e.appendChild(createElement(child))
    }

    return e;
}

function hasDiff(a, b) {
    if (typeof a !== typeof b)
        return DiffType.Type

    if (!isNode(a) && a !== b)
        return DiffType.Text

    if (isNode(a) && isNode(b)) {
        if (a.nodeName !== b.nodeName)
            return DiffType.Node
        
        if (a.attributes.value !== b.attributes.value)
            return DiffType.Value

        if (JSON.stringify(a.attributes) !== JSON.stringify(b.attributes))
            return DiffType.Attribute
    }
    
    return DiffType.None
}

function updateElement(parent, oldNode, newNode, index = 0) {
    if (oldNode == null) {
        parent.appendChild(createElement(newNode))
        return
    }

    const target = parent.childNodes[index]

    if (newNode == null) {
        parent.removeChild(target)
        return
    }

    const diffType = hasDiff(oldNode, newNode)
    switch (diffType) {
        case DiffType.Type:
        case DiffType.Text:
        case DiffType.Node:
            parent.replaceChild(createElement(newNode), target)
            return
        case DiffType.Value:
            updateValue(target, newNode.attributes.value)
            return
        case DiffType.Attribute:
            updateAttributes(target, oldNode.attributes, newNode.attributes)
            return
    }

    if (isNode(oldNode) && isNode(newNode)) {
        for (let i = 0; i < newNode.children.length || i < oldNode.children.length; i++) {
            updateElement(target, oldNode.children[i], newNode.children[i], i)
        }
    }
}

function updateAttributes(target, oldAttributes, newAttributes) {
    for (let attribute in oldAttributes) {
        if (!isEventAttribute(attribute)) {
            target.removeAttribute(attribute)
        }
    }
    for (let attribute in newAttributes) {
        if (!isEventAttribute(attribute)) {
            target.setAttribute(attribute, newAttributes[attribute])
        }
    }
}

function updateValue(target, newValue) {
    target.value = newValue
}

function isNode(node) {
    return typeof node !== "string" && typeof node !== "number"
}

function isEventAttribute(attribute) {
    return /^on/.test(attribute)
}

class App {
    constructor(e, view, state, actions) {
        this.e = typeof e === "string" ? document.querySelector(e) : e
        this.view = view
        this.state = state
        this.actions = this.dispatchAction(actions)
        this.resolveNode()
    }

    dispatchAction(actions) {
        const dispatched = {}
        for (let key in actions) {
            const action = actions[key]
            dispatched[key] = (state, ...data) => {
                const result = action(state, ...data)
                this.resolveNode()
                return result
            }
        }
        return dispatched
    }

    resolveNode() {
        this.newNode = this.view(this.state, this.actions)
        this.render()
    }

    render() {
        if (this.oldNode != null) {
            updateElement(this.e, this.oldNode, this.newNode)
        } else {
            this.e.appendChild(createElement(this.newNode))
        }

        this.oldNode = this.newNode
    }
}

const state = {
    tasks: [],
    form: {
        input: "",
        hasError: false
    }
}

const actions = {
    validate: (state, input) => {
        if (!input || input.length < 3 || input.length > 20) {
            state.form.hasError = true
        } else {
            state.form.hasError = false
        }

        return !state.form.hasError
    },
    createTask: (state, title) => {
        state.tasks.push(title)
        state.form.input = ""
    },
    removeTask: (state, index) => {
        state.tasks.splice(index, 1)
    }
}

const view = (state, actions) => {
    return h("div", { style: "padding:20px" }, [
        h("h1", { class: "title" }, ["TODO"]),
        h("div", { class: "field" }, [
            h("label", { class: "label" }, ["Task title"]),
            h("input", {
                type: "text",
                class: "input",
                style: "width:200px",
                value: state.form.input,
                oninput: (e) => {
                    state.form.input = e.target.value
                    actions.validate(state, state.form.input)
                }
            }, []),
            h("button", {
                type: "button",
                class: "button is-primary",
                style: "margin-left:10px",
                onclick: () => {
                    if (actions.validate(state, state.form.input)) {
                        actions.createTask(state, state.form.input)
                    }
                }
            }, ["create"]),
            h("p", {
                class: "notification",
                style: `display: ${state.form.hasError ? "display" : "none"}`
            }, ["3～20文字で入力してください"])
        ]),
        h("ul", { class: "panel" }, state.tasks.map((task, i) => {
            return h("li", { class: "panel-block" }, [
                h("button", {
                    class: "button",
                    class: "delete",
                    style: "margin-right:10px",
                    onclick: () => actions.removeTask(state, i)
                }, ["remove"]),
                task
            ])
        }))
    ])
}

new App("#app", view, state, actions)
